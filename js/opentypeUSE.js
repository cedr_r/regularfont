var papa = document.getElementById("lettershow");
var listg = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "+", "<", "=", ">", ",", ".", "/", ";", ":", "?", "@", "\\", "&", "%", "*", "!", "-", "_", "(", "{", "[", ")", "}", "]"]
var BigLetter = document.getElementById("BigLetter");
var basefont = 'fonts/RegTimes.otf';
var initLet = 'A';
var ccletter;

var colorChange = ["rgb(70, 255, 116)","blueviolet","rgb(188, 116, 255)","rgb(255, 0, 255)","rgb(65, 61, 255)","rgb(137, 137, 255)","rgb(255, 58, 58)"]



function LittleLetter(bfont){

opentype.load(bfont, function (err, font){
  papa.innerHTML='';
  for (var i = 0; i < listg.length; i++) {
    var canvas = document.createElement('canvas');
    var thID = listg[i]

    canvas.id = thID; 
    canvas.className = "cletter"; 
    var z = listg[i];
    if (document.getElementById(thID) != null){
      document.getElementById(thID).addEventListener("mouseenter",MegLetter(z,font))
    }
    canvas.width = 100;
    canvas.height = 100;
    canvas.style.position = "relative";
    canvas.style.position = "inline-block";
    canvas.style.borderRadius = "25px";
    papa.appendChild(canvas)
    
    const ctx = canvas.getContext("2d");
   
    var dims = Raphael.pathBBox(font.getPath(listg[i], 0, 60, 70).toPathData());
    const path = font.getPath(listg[i],((100-dims.width)/2), 70, 70);
    if(resp){
      path.fill = "white";
    }else{
    path.fill = "black";
    } 
    path.draw(ctx)
  }

  ccletter = Array.from(document.getElementsByClassName("cletter"));
  ccletter.forEach(lettt => {
    lettt.addEventListener("mouseenter",function(){
      var bt = lettt.id;
      let root = document.documentElement;
      const random = Math.floor(Math.random() * colorChange.length);
      root.style.setProperty('--eleCase', colorChange[random]);
      MegLetter(bt,bfont);
    })
  })

})
}

function MegLetter(inLet,bfont){
  opentype.load(bfont, function (err, font){
    const ctx = BigLetter.getContext("2d");
    if(resp){
      BigLetter.width = (window.innerWidth)-((window.innerWidth/20)*2);
      BigLetter.height = (window.innerHeight/3*2)-((window.innerWidth/20)*2);
    }else{
      BigLetter.width = (window.innerWidth/2)-((window.innerWidth/20)*2);
      BigLetter.height = window.innerHeight-((window.innerWidth/20)*2);
      
    }

    ctx.clearRect(0, 0, window.innerWidth-((window.innerWidth/20)*2), window.innerHeight-((window.innerWidth/20)*2));

    if(resp){
      var dims = Raphael.pathBBox(font.getPath(inLet, 0, window.innerHeight/2.8, window.innerHeight/2.5).toPathData());
      var xPos = (window.innerWidth-((window.innerWidth/20)*2)-dims.width)/2
      var path = font.getPath(inLet, xPos,  window.innerHeight/2.8, window.innerHeight/2.5);
      font.drawMetrics(ctx, inLet, xPos, window.innerHeight/2.8, window.innerHeight/2.5);
    }else{
      var dims = Raphael.pathBBox(font.getPath(inLet, 0, window.innerHeight/2, 600).toPathData());
      var xPos = ((window.innerWidth/2)-((window.innerWidth/20)*2)-dims.width)/2
      var path = font.getPath(inLet, xPos, window.innerHeight/2, 600);
      font.drawMetrics(ctx, inLet, xPos, window.innerHeight/2, 600);
    }
    path.fill = "red";
    path.draw(ctx);
  })
}



LittleLetter(basefont);
MegLetter(initLet,basefont);

window.addEventListener("resize",MegLetter(initLet,basefont));
window.addEventListener("resize",LittleLetter(basefont));


var ct = Array.from(document.getElementsByClassName("ct"))

ct.forEach(cee => {

    cee.addEventListener("click",function(){
        var txte = cee.textContent;
        var nfont = 'fonts/' + txte;

        ct.forEach(alll => {
          alll.style.color = "rgb(255, 58, 58)";
          alll.style.backgroundColor = "white";
        })
        
        cee.style.color = "white";
        cee.style.backgroundColor = "rgb(255, 58, 58)";
        LittleLetter(nfont);
        MegLetter(initLet,nfont);

    })
});

