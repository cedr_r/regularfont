var posX;
var posY;

var resp = false;

function checkResp(){
if(window.innerWidth < window.innerHeight){
    resp = true;
}else{
    resp = false;
}
}

checkResp();

window.addEventListener("resize",checkResp);

window.addEventListener('mousemove',function(e){
    posX = e.clientX;
    posY = e.clientY;
})


function moveSize(){
    document.getElementById('size').style.fontSize = "calc(2vw + "+ posX/35 + "vw)";
    document.getElementById("ValS").innerHTML = parseInt(posX/innerWidth*100,0);

}

//window.addEventListener("mousemove",moveSize)

var cf = Array.from(document.getElementsByClassName("cf"))

cf.forEach(mee => {
    mee.addEventListener("click",function(){
        var txt = mee.textContent;
        document.getElementById('typeTest').style.fontFamily = txt;
        cf.forEach(e =>{
            e.style.backgroundColor = "white";
            e.style.color = "blue";
        })
        mee.style.color = "white";
        mee.style.backgroundColor ="blue";

    })
});

var CFTT = Array.from(document.getElementsByClassName("CFTT"))

CFTT.forEach(mee => {
    mee.addEventListener("click",function(){
      if(CFTT.indexOf(mee) == 0){
        document.getElementById("typetype").style.fontFamily = "TimesR";
      }else if(CFTT.indexOf(mee) == 1){
        document.getElementById("typetype").style.fontFamily = "TimesRi"
      }else if(CFTT.indexOf(mee) == 2){
        document.getElementById("typetype").style.fontFamily = "StandR"
      }else {
        document.getElementById("typetype").style.fontFamily = "DurandusR"
    }

    for (var i = 0;i<CFTT.length;i++){
        CFTT[i].style.color = "white";
        CFTT[i].style.backgroundColor = "transparent";
    }

    mee.style.backgroundColor = "white";
    mee.style.color = "rgb(188, 116, 255)"

    })
});

function resetResize(){
    if(aboutD == true){
        if(resp){
            aboutC.style.maxHeight = "100vh";
            aboutC.style.padding = "5vw"
        }else{
            aboutC.style.maxHeight = "100vh";
            aboutC.style.padding = "2vw"
        }
        
    }else{
        if(resp){
            aboutC.style.maxHeight = "0";
            aboutC.style.padding = "0 5vw"
        }else{
             aboutC.style.maxHeight = "0";
            aboutC.style.padding = "0 2vw" 
        }
        
    }
}

var about = document.getElementById("about");
var aboutC = document.getElementById("AboutC");
window.addEventListener("resize",resetResize)
var aboutD = false;

about.addEventListener("click",function(){
    aboutD = !aboutD;
    resetResize();
})

var t01 =  document.getElementById("testerOne");
var t02 =  document.getElementById("testerTwo");
t01.addEventListener("input",function(){
    const toTT = t01.textContent;
    t02.innerHTML = toTT;
});
t02.addEventListener("input",function(){
    const toTT = t02.textContent;
    t01.innerHTML = toTT;
})


var FtS = Array.from(document.getElementsByClassName("FtS"))

FtS.forEach(ffts => {
    ffts.addEventListener("click",function(){
        const ttf = FtS.indexOf(ffts);
        if(ttf == 0){
            t01.style.fontFamily = "FreeS";
            t02.style.fontFamily = "TimesR";
            if(resp){
                t02.style.lineHeight = "2.7vh";
            }else{
                t02.style.lineHeight = "27px";
            }
        }else if(ttf == 1){
            t01.style.fontFamily = "FreeSi";
            t02.style.fontFamily = "TimesRI";
            if(resp){
                t02.style.lineHeight = "2.7vh";
            }else{
                t02.style.lineHeight = "27px";
            }
        }
        else if(ttf == 2){
            t01.style.fontFamily = "Fust";
            t02.style.fontFamily = "DurandusR";
            if(resp){
                t02.style.lineHeight = "2.6vh";
            }else{
                t02.style.lineHeight = "26px";
            }
        }
        else if(ttf == 3){
            t01.style.fontFamily = "Standard";
            t02.style.fontFamily = "StandR";
            if(resp){
                t02.style.lineHeight = "3vh";
            }else{
                t02.style.lineHeight = "30px";
            }
        }

        FtS.forEach(ff => {
            ff.style.backgroundColor = "rgb(65, 61, 255)";
            ff.style.color = "white";
        })

        ffts.style.color = "rgb(65, 61, 255)";
        ffts.style.backgroundColor = "white";


    })
})